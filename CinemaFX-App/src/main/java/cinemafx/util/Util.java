package cinemafx.util;

import javafx.scene.control.TextFormatter;
import javafx.scene.paint.Color;
import javafx.util.converter.NumberStringConverter;

import java.text.ParsePosition;
import java.util.function.UnaryOperator;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 11/04/2017.
 */
public class Util {

    public static int percentage(int max, int actual) {
        return actual * 100 / max;
    }

    public static Color percentageToColor(int percentage) {
        if (percentage < 10) return Color.RED;
        if (percentage < 25) return Color.ORANGE;
        if (percentage < 50) return Color.YELLOW;
        return Color.GREEN;
    }

    public class NumberStringFilteredConverter extends NumberStringConverter {
        // Note, if needed you can add in appropriate constructors
        // here to set locale, pattern matching or an explicit
        // type of NumberFormat.
        //
        // For more information on format control, see
        //    the NumberStringConverter constructors
        //    DecimalFormat class
        //    NumberFormat static methods for examples.
        // This solution can instead extend other NumberStringConverters if needed
        //    e.g. CurrencyStringConverter or PercentageStringConverter.

        public UnaryOperator<TextFormatter.Change> getFilter() {
            return change -> {
                String newText = change.getControlNewText();
                if (newText.isEmpty()) {
                    return change;
                }

                ParsePosition parsePosition = new ParsePosition( 0 );
                Object object = getNumberFormat().parse( newText, parsePosition );
                if ( object == null || parsePosition.getIndex() < newText.length()) {
                    return null;
                } else {
                    return change;
                }
            };
        }
    }
}

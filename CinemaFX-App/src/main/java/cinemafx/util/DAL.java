package cinemafx.util;

import cinemafx.model.Schedule;
import cinemafx.model.Status;
import cinemafx.persistence.io.ScheduleFileDriver;
import cinemafx.persistence.io.StatusFileDriver;

import java.io.File;
import java.io.InputStream;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 11/04/2017.
 */
public class DAL {
    private final static StatusFileDriver STATUS_FILE_DRIVER;
    private final static Schedule SCHEDULE;
    private final static Status STATUS;
    private final static File STATUS_FILE;
    static {
        ScheduleFileDriver  scheduleFileDriver = new ScheduleFileDriver();
        InputStream is = ClassLoader.getSystemResourceAsStream("data/schedule.xml");
        SCHEDULE = scheduleFileDriver.load(is);
        STATUS_FILE_DRIVER= new StatusFileDriver(SCHEDULE);
//        is = ClassLoader.getSystemResourceAsStream("data/status.xml");
//        STATUS = STATUS_FILE_DRIVER.load(is);
        STATUS_FILE = new File(ClassLoader.getSystemResource("data/status.xml").getFile());
        STATUS = STATUS_FILE_DRIVER.load(STATUS_FILE);
    }

    public static Schedule getSchedule() {
        return SCHEDULE;
    }

    public static Status getStatus() {
        return STATUS;
    }

    public static void updateStatus() {
        STATUS_FILE_DRIVER.save(STATUS, STATUS_FILE);
    }
}

package cinemafx.util;

import cinemafx.model.Schedule;
import cinemafx.model.Session;
import cinemafx.model.Showing;
import cinemafx.model.Status;
import cinemafx.persistence.io.ScheduleFileDriver;
import cinemafx.persistence.io.StatusFileDriver;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 20/03/2017.
 */
public class GenerateStatus {
    public static void main(String[] args) throws JAXBException {
        InputStream is = ClassLoader.getSystemResourceAsStream("data/schedule.xml");
        ScheduleFileDriver scheduleFileDriver = new ScheduleFileDriver();
        Schedule schedule = scheduleFileDriver.load(is);
        Status status = new Status();
        for (Showing showing : schedule.getShowings()) {
            Session session = new Session(showing);
            status.getSessions().add(session);
        }
        Path path = null;
        File file = null;
        try {
            path = Paths.get(System.getProperty("user.home") + File.separator + "status.xml");
            file = Files.createFile(path).toFile();
        } catch (FileAlreadyExistsException e) {
            System.err.println("File already exists: " + path);
            file = path.toFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        StatusFileDriver statusFileDriver = new StatusFileDriver(schedule);
        statusFileDriver.save(status, file);
    }
}

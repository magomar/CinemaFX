package cinemafx.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Mario
 */
public final class Showcase<T> {

    private final List<T> list;
    private int windowSize;
    private int base, top;
    private final List<T> showcase;

    public Showcase(Collection<T> collection, int windowSize) {
        this.list = new ArrayList<>(collection);
        this.windowSize = windowSize;
        base = 0;
        top = base + windowSize - 1;
        showcase = new ArrayList<>(windowSize);
    }


    public void prev() {
        top--;
        base--;
        if (top == -1) {
            top = list.size()-1;
        } else
        if (base == -1) {
            base = list.size()-1;
        }
    }

    public void next() {
        top++;
        base++;
        if (top == list.size()) {
            top = 0;
        } else
        if (base == list.size()) {
            base = 0;
        }
    }

    public List<T> getShowcaseList() {
        showcase.clear();
        if (top > base) {
            showcase.addAll(list.subList(base, top + 1));
        } else {
            showcase.addAll(list.subList(base, list.size()));
            showcase.addAll(list.subList(0, top+1));
        }
        return showcase;
    }

    public int getBase() {
        return base;
    }

    public int getTop() {
        return top;
    }

}
package cinemafx.controller;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 05/04/2017.
 */

import cinemafx.model.Movie;
import cinemafx.model.Schedule;
import cinemafx.model.Showcase;
import cinemafx.persistence.io.ScheduleFileDriver;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.image.ImageView;

import java.io.InputStream;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

public class MoviesController implements Initializable {

    @FXML
    private DatePicker date;

    @FXML
    private ImageView imageView1;

    @FXML
    private ImageView imageView2;

    @FXML
    private ImageView imageView3;

    private Schedule schedule;
    private Collection<Movie> movies;
    private Showcase<Movie> showcase;

    private void updatePosters() {
        List<Movie> visibleMovies = showcase.getShowcaseList();
        imageView1.setImage(visibleMovies.get(0).getPoster());
        imageView2.setImage(visibleMovies.get(1).getPoster());
        imageView3.setImage(visibleMovies.get(2).getPoster());
    }


    @FXML
    private void previousMovie(ActionEvent event) {
        showcase.prev();
        updatePosters();
    }

    @FXML
    private void nextMovie(ActionEvent event) {
        showcase.next();
        updatePosters();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ScheduleFileDriver scheduleFileDriver = new ScheduleFileDriver();
        InputStream is = ClassLoader.getSystemResourceAsStream("data/schedule.xml");
        schedule = scheduleFileDriver.load(is);

        movies = schedule.getMovies().values();
        showcase = new Showcase<>(movies, 3);
        updatePosters();
    }
}
package cinemafx.controller;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 * Created on 10/04/2017.
 */

import cinemafx.model.Movie;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class MovieController implements Initializable {

    @FXML
    private ImageView poster;

    @FXML
    private TextArea description;

    private Movie movie;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    public void setMovie(Movie movie) {
        this.movie = movie;
        description.setText(movie.toMultilineString());
        poster.setImage(movie.getPoster());
    }
}
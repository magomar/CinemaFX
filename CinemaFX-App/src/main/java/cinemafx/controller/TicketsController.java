package cinemafx.controller;

import cinemafx.model.Seat;
import cinemafx.model.Session;
import cinemafx.util.DAL;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class TicketsController implements Initializable {

    private static final int ROWS = 18;
    private static final int COLUMNS = 12;

    @FXML
    private HBox roomPanel;
    @FXML
    private SessionController sessionController;

    private final CheckBox[][] seatStatus = new CheckBox[ROWS][COLUMNS];
    private Session session;
    private Seat[][] seats = new Seat[ROWS][COLUMNS];

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        GridPane room = new GridPane();
        for (int i = 0; i <= ROWS; i++) {
            room.addRow(i);
        }
        for (int j = 0; j <= COLUMNS; j++) {
            room.addColumn(j);
        }
        for (int i = 0; i <= ROWS; i++) {
            room.add(new Label(String.valueOf(i)), 0, i);
        }
        for (int j = 0; j <= COLUMNS; j++) {
            room.add(new Label(String.valueOf(j)), j, 0);
        }
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                seats[i][j] = new Seat(i,j);
                seatStatus[i][j] = new CheckBox();
                seats[i][j].occupiedProperty().bind(seatStatus[i][j].selectedProperty());
                room.add(seatStatus[i][j], j + 1, i + 1);
            }
        }
        roomPanel.getChildren().add(room);
    }

    public void setSession(Session session) {
        this.session = session;
        sessionController.setSession(session);
        boolean[][] occupation = session.getOccupation();
        for (int i = 0; i < ROWS; i++) {
            for (int j = 0; j < COLUMNS; j++) {
                seatStatus[i][j].setSelected(occupation[i][j]);
            }
        }
    }

    @FXML
    private void confirm(ActionEvent event) {
        session.updateSeats(seats);
        DAL.updateStatus();
        roomPanel.getScene().getWindow().hide();
    }

    @FXML
    private void cancel(ActionEvent event) {
        roomPanel.getScene().getWindow().hide();
    }


}

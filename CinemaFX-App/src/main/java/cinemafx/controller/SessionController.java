package cinemafx.controller;

import cinemafx.model.Session;
import cinemafx.util.Util;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.shape.Circle;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 10/04/2017.
 */

public class SessionController implements Initializable {

    @FXML
    private Label details;
    @FXML
    private Label seatsAvailable;

    @FXML
    private MovieController movieController;

    private Session session;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setSession(Session session) {
        this.session = session;
        movieController.setMovie(session.getMovie());
        details.setText(session.getDate() + "   " + session.getTime() + "   Room " + session.getRoom().getName());
        seatsAvailable.setText(String.valueOf(session.getAvailability()));
        int percentage = Util.percentage(session.getRows() * session.getColumns(), session.getAvailability());
        seatsAvailable.setGraphic(new Circle(6, Util.percentageToColor(percentage)));
    }

}
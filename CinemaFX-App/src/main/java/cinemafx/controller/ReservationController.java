package cinemafx.controller;

import cinemafx.model.Reservation;
import cinemafx.model.Schedule;
import cinemafx.model.Session;
import cinemafx.model.Status;
import cinemafx.persistence.io.ScheduleFileDriver;
import cinemafx.persistence.io.StatusFileDriver;
import cinemafx.util.DAL;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.stage.Stage;
import javafx.util.StringConverter;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 07/04/2017.
 */
public class ReservationController implements Initializable {
    private static final NumberFormat INTEGER_FORMAT = NumberFormat.getIntegerInstance();
    @FXML
    private TextField name;

    @FXML
    private TextField phone;

    @FXML
    private TextField numSeats;

    @FXML
    private SessionController sessionController;

    private Session session;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        UnaryOperator<TextFormatter.Change> numSeatsFilter = change -> {
            if (change.getControlNewText().isEmpty()) {
                return change;
            }

            ParsePosition parsePosition = new ParsePosition(0);
            Number number = INTEGER_FORMAT.parse(change.getControlNewText(), parsePosition);

            if (number == null
                    || parsePosition.getIndex() < change.getControlNewText().length()
                    || number.intValue() > session.getAvailability()) {
                return null;
            } else {
                return change;
            }
        };
        numSeats.setTextFormatter(new TextFormatter<>(numSeatsFilter));
        phone.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().isEmpty()) {
                return change;
            }

            ParsePosition parsePosition = new ParsePosition(0);
            Number number = INTEGER_FORMAT.parse(change.getControlNewText(), parsePosition);

            if (number == null
                    || parsePosition.getIndex() < change.getControlNewText().length()
                    || parsePosition.getIndex() > 9) {
                return null;
            } else {
                return change;
            }
        }));
    }

    public void setSession(Session session) {
        this.session = session;
        sessionController.setSession(session);
    }


    @FXML
    private void confirm(ActionEvent event) {
        if (name.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Warning");
            alert.setHeaderText("Name missing");
            alert.setContentText("Please enter a contact name !");
            alert.showAndWait();
        } else if (phone.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Warning");
            alert.setHeaderText("Phone missing");
            alert.setContentText("Please enter a contact phone number !");
            alert.showAndWait();
        } else if (numSeats.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Warning");
            alert.setHeaderText("Number of seats is missing");
            alert.setContentText("Please specify the amount of seats to be reserved !");
            alert.showAndWait();
        } else {
            Reservation reservation = new Reservation(name.getText(), phone.getText(), Integer.parseInt(numSeats.getText()));
            session.addReservation(reservation);
            name.getScene().getWindow().hide();
            DAL.updateStatus();
            clearData();
        }

//        else if (Integer.parseInt(numSeats.getText()) > session.getAvailability()) {
//            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            alert.setTitle("Warning");
//            alert.setHeaderText("Not enough seats available");
//            alert.setContentText("Please reduce the number of seats or try another session !");
//            alert.showAndWait();
//        }

    }

    @FXML
    private void cancel(ActionEvent event) {
        name.getScene().getWindow().hide();
        clearData();
    }

    private void clearData() {
        name.clear();
        phone.clear();
        numSeats.clear();
    }
}

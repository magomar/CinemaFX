package cinemafx.controller;

import cinemafx.model.Movie;
import cinemafx.model.Schedule;
import cinemafx.model.Session;
import cinemafx.model.Status;
import cinemafx.util.DAL;
import cinemafx.util.Util;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Circle;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ResourceBundle;
import java.util.function.Predicate;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 */
public class SessionsController implements Initializable {

    @FXML
    private DatePicker from;
    @FXML
    private DatePicker to;
    @FXML
    private Button clearFromButton;
    @FXML
    private Button fromToButton;
    @FXML
    private Button clearToButton;
    @FXML
    private Button toFromButton;
    @FXML
    private ListView<Movie> movieList;
    @FXML
    private TableColumn<Session, LocalDate> dateCol;
    @FXML
    private TableColumn<Session, LocalTime> timeCol;
    @FXML
    private TableColumn<Session, String> roomCol;
    @FXML
    private TableColumn<Session, String> titleCol;
    @FXML
    private TableColumn<Session, Integer> seatsCol;
    @FXML
    private TableView<Session> sessionsTable;

    private Schedule schedule;
    private Status status;
    private final ObservableList<Movie> movies = FXCollections.observableArrayList();
    private final ObservableList<Session> sessions = FXCollections.observableArrayList();
    private final Predicate<Session> filteringPredicate = new FilteringPredicate();
    private final FilteredList<Session> filteredSessions = new FilteredList<>(sessions);
    private Stage reservationStage;
    private ReservationController reservationController;
    private Stage ticketsStage;
    private TicketsController ticketsController;


    @Override
    public void initialize(URL url, ResourceBundle rb) {

        movieList.setCellFactory(c -> new MovieListingCell());
        dateCol.setCellValueFactory(new PropertyValueFactory<>("date"));
        timeCol.setCellValueFactory(new PropertyValueFactory<>("time"));
        roomCol.setCellValueFactory(new PropertyValueFactory<>("room"));
        titleCol.setCellValueFactory(new PropertyValueFactory<>("movie"));
        seatsCol.setCellValueFactory(new PropertyValueFactory<>("availability"));
        seatsCol.setCellFactory(c -> new SessionSeatsTableCell());

        filteredSessions.setPredicate(new FilteringPredicate());

        SortedList<Session> sortedSessions = new SortedList<>(filteredSessions);
        sortedSessions.comparatorProperty().bind(sessionsTable.comparatorProperty());

        movieList.setItems(movies);
        sessionsTable.setItems(sortedSessions);

        from.valueProperty().addListener(this::resetFiltering);
        to.valueProperty().addListener(this::resetFiltering);
        movieList.getSelectionModel().selectedItemProperty().addListener(this::resetFiltering);

        ContextMenu contextMenu = createSessionContextMenu();
        sessionsTable.setRowFactory(tv -> {
            TableRow<Session> row = new TableRow<>();
            // only display context menu for non-null items:
            row.contextMenuProperty().bind(
                    Bindings.when(Bindings.isNotNull(row.itemProperty()))
                            .then(contextMenu)
                            .otherwise((ContextMenu) null));
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty()) {
                    Session session = row.getItem();
                    if (event.getClickCount() == 2) { //double click
                        buyTickets(null);
                    }
                }
            });
            return row;
        });
        this.schedule = DAL.getSchedule();
        this.status = DAL.getStatus();
        movies.setAll(schedule.getMovies().values());
        sessions.setAll(status.getSessions());
    }

    private ContextMenu createSessionContextMenu() {
        MenuItem reserve = new MenuItem("Reserve tickets");
        reserve.setOnAction(this::reserveTickets);
        MenuItem buy = new MenuItem("Buy tickets");
        buy.setOnAction(this::buyTickets);
        ContextMenu contextMenu = new ContextMenu(reserve, buy);
        return contextMenu;
    }


    private void reserveTickets(ActionEvent event) {
        if (reservationStage == null) {
            reservationStage = new Stage();
            reservationController =loadModalView("view/Reservation.fxml", reservationStage);
        }
        Session session = sessionsTable.getSelectionModel().getSelectedItem();
        reservationController.setSession(session);
        reservationStage.showAndWait();
        sessionsTable.refresh();
    }

    private void buyTickets(ActionEvent event) {
        if (ticketsStage == null) {
            ticketsStage = new Stage();
            ticketsController = loadModalView("view/Tickets.fxml", ticketsStage);
        }
        Session session = sessionsTable.getSelectionModel().getSelectedItem();
        ticketsController.setSession(session);
        ticketsStage.showAndWait();
        sessionsTable.refresh();
    }

    private <T> T loadModalView(String fxmlRelativePath, Stage stage) {
        FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource(fxmlRelativePath));
        T controller = null;
        try {
            Parent root = loader.load();
            Scene reservationScene = new Scene(root);
            controller = loader.getController();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(reservationScene);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }

    @FXML
    void clearMovie(ActionEvent event) {
        movieList.getSelectionModel().clearSelection();
    }

    @FXML
    void clearFrom(ActionEvent event) {
        from.setValue(null);
    }

    @FXML
    void clearTo(ActionEvent event) {
        to.setValue(null);
    }

    @FXML
    void copyFromTo(ActionEvent event) {
        to.setValue(from.getValue());
    }

    @FXML
    void copyToFrom(ActionEvent event) {
        from.setValue(to.getValue());
    }

    private void resetFiltering(Observable observable) {
        filteredSessions.setPredicate(p -> false);
        filteredSessions.setPredicate(filteringPredicate);
    }

    class FilteringPredicate implements Predicate<Session> {

        private final Predicate<Session> compositePredicate;

        public FilteringPredicate() {
            compositePredicate = new FromFilteringPredicate()
                    .and(new ToFilteringPredicate())
                    .and(new MovieFilteringPredicate());
        }

        @Override
        public boolean test(Session t) {
            return compositePredicate.test(t);
        }
    }

    class FromFilteringPredicate implements Predicate<Session> {

        @Override
        public boolean test(Session t) {
            LocalDate date = from.getValue();
            if (date != null) {
                return t.getDate().isAfter(date)
                        || t.getDate().isEqual(date);
            } else {
                return true;
            }
        }
    }

    class ToFilteringPredicate implements Predicate<Session> {

        @Override
        public boolean test(Session t) {
            LocalDate date = to.getValue();
            if (date != null) {
                return t.getDate().isBefore(date)
                        || t.getDate().isEqual(date);
            } else {
                return true;
            }
        }
    }

    class MovieFilteringPredicate implements Predicate<Session> {

        @Override
        public boolean test(Session t) {
            Movie selectedMovie = movieList.getSelectionModel().getSelectedItem();
            if (selectedMovie != null) {
                return t.getMovie().equals(selectedMovie);
            } else {
                return true;
            }
        }
    }

    class MovieListingCell extends ListCell<Movie> {
        final ImageView view = new ImageView();
        final Tooltip tooltip = new Tooltip();

        @Override
        protected void updateItem(Movie item, boolean empty) {
            super.updateItem(item, empty);
            if (item == null || empty) {
                setText(null);
                setGraphic(null);
            } else {
                tooltip.setText(item.toMultilineString());
                setTooltip(tooltip);
                Image image = item.getPoster();
                view.setFitWidth(image.getWidth() / 4);
                view.setFitHeight(image.getHeight() / 4);
                view.setImage(image);
                setGraphic(view);
                setText(item.getTitle());
            }

        }
    }

    class SessionSeatsTableCell extends TableCell<Session, Integer> {

        @Override
        protected void updateItem(Integer item, boolean empty) {
            super.updateItem(item, empty);
            TableRow<Session> currentRow = getTableRow();
            Session session = currentRow.getItem();
            if (session == null || empty) {
                setText(null);
                setGraphic(null);
            } else {
                setText(item.toString());
                int percentage = Util.percentage(session.getRows() * session.getColumns(), item);
                setGraphic(new Circle(6, Util.percentageToColor(percentage)));
            }

        }
    }

}

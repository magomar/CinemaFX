package cinemafx.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author margomez
 */
public class CinemaFX extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(ClassLoader.getSystemResource("view/MainView.fxml"));
        
        Scene scene = new Scene(root);
        scene.getStylesheets().add("css/cinemafx.css");

        stage.setScene(scene);
        
        //stage.initStyle(StageStyle.UNDECORATED);
        
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}

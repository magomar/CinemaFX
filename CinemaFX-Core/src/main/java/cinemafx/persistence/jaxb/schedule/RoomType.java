
package cinemafx.persistence.jaxb.schedule;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para roomType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="roomType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rows" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="seats" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "roomType", propOrder = {
    "rows",
    "seats",
    "name"
})
public class RoomType {

    protected int rows;
    protected int seats;
    @XmlElement(required = true)
    protected String name;

    /**
     * Obtiene el valor de la propiedad rows.
     * 
     */
    public int getRows() {
        return rows;
    }

    /**
     * Define el valor de la propiedad rows.
     * 
     */
    public void setRows(int value) {
        this.rows = value;
    }

    /**
     * Obtiene el valor de la propiedad seats.
     * 
     */
    public int getSeats() {
        return seats;
    }

    /**
     * Define el valor de la propiedad seats.
     * 
     */
    public void setSeats(int value) {
        this.seats = value;
    }

    /**
     * Obtiene el valor de la propiedad name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Define el valor de la propiedad name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}

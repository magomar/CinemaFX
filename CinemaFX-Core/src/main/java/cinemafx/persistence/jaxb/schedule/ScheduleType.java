
package cinemafx.persistence.jaxb.schedule;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para scheduleType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="scheduleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rooms" type="{}roomsType"/>
 *         &lt;element name="movies" type="{}moviesType"/>
 *         &lt;element name="showings" type="{}showingsType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "scheduleType", propOrder = {
    "rooms",
    "movies",
    "showings"
})
public class ScheduleType {

    @XmlElement(required = true)
    protected RoomsType rooms;
    @XmlElement(required = true)
    protected MoviesType movies;
    @XmlElement(required = true)
    protected ShowingsType showings;

    /**
     * Obtiene el valor de la propiedad rooms.
     * 
     * @return
     *     possible object is
     *     {@link RoomsType }
     *     
     */
    public RoomsType getRooms() {
        return rooms;
    }

    /**
     * Define el valor de la propiedad rooms.
     * 
     * @param value
     *     allowed object is
     *     {@link RoomsType }
     *     
     */
    public void setRooms(RoomsType value) {
        this.rooms = value;
    }

    /**
     * Obtiene el valor de la propiedad movies.
     * 
     * @return
     *     possible object is
     *     {@link MoviesType }
     *     
     */
    public MoviesType getMovies() {
        return movies;
    }

    /**
     * Define el valor de la propiedad movies.
     * 
     * @param value
     *     allowed object is
     *     {@link MoviesType }
     *     
     */
    public void setMovies(MoviesType value) {
        this.movies = value;
    }

    /**
     * Obtiene el valor de la propiedad showings.
     * 
     * @return
     *     possible object is
     *     {@link ShowingsType }
     *     
     */
    public ShowingsType getShowings() {
        return showings;
    }

    /**
     * Define el valor de la propiedad showings.
     * 
     * @param value
     *     allowed object is
     *     {@link ShowingsType }
     *     
     */
    public void setShowings(ShowingsType value) {
        this.showings = value;
    }

}

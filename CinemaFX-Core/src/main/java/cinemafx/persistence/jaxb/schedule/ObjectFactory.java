
package cinemafx.persistence.jaxb.schedule;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cinemafx.persistence.jaxb.schedule package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Schedule_QNAME = new QName("", "schedule");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cinemafx.persistence.jaxb.schedule
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ScheduleType }
     * 
     */
    public ScheduleType createScheduleType() {
        return new ScheduleType();
    }

    /**
     * Create an instance of {@link MovieType }
     * 
     */
    public MovieType createMovieType() {
        return new MovieType();
    }

    /**
     * Create an instance of {@link MoviesType }
     * 
     */
    public MoviesType createMoviesType() {
        return new MoviesType();
    }

    /**
     * Create an instance of {@link RoomsType }
     * 
     */
    public RoomsType createRoomsType() {
        return new RoomsType();
    }

    /**
     * Create an instance of {@link RoomType }
     * 
     */
    public RoomType createRoomType() {
        return new RoomType();
    }

    /**
     * Create an instance of {@link ShowingsType }
     * 
     */
    public ShowingsType createShowingsType() {
        return new ShowingsType();
    }

    /**
     * Create an instance of {@link ShowingType }
     * 
     */
    public ShowingType createShowingType() {
        return new ShowingType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ScheduleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "schedule")
    public JAXBElement<ScheduleType> createSchedule(ScheduleType value) {
        return new JAXBElement<ScheduleType>(_Schedule_QNAME, ScheduleType.class, null, value);
    }

}

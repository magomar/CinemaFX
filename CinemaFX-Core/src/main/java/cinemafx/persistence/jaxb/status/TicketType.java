
package cinemafx.persistence.jaxb.status;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ticketType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ticketType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="row" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="seat" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ticketType", propOrder = {
    "row",
    "seat"
})
public class TicketType {

    protected int row;
    protected int seat;

    /**
     * Obtiene el valor de la propiedad row.
     * 
     */
    public int getRow() {
        return row;
    }

    /**
     * Define el valor de la propiedad row.
     * 
     */
    public void setRow(int value) {
        this.row = value;
    }

    /**
     * Obtiene el valor de la propiedad seat.
     * 
     */
    public int getSeat() {
        return seat;
    }

    /**
     * Define el valor de la propiedad seat.
     * 
     */
    public void setSeat(int value) {
        this.seat = value;
    }

}

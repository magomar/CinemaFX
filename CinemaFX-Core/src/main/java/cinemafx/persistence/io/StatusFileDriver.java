package cinemafx.persistence.io;

import cinemafx.model.*;
import cinemafx.persistence.jaxb.status.*;
import cinemafx.util.DateTimeUtils;

import javax.xml.bind.JAXBElement;
import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 03/04/2017.
 */
public class StatusFileDriver extends AbstractFileDriver<StatusType, Status> {
    private Schedule schedule;
    private ObjectFactory objectFactory = new ObjectFactory();

    public StatusFileDriver(Schedule schedule) {
        super("cinemafx.persistence.jaxb.status", StatusType.class);
        this.schedule = schedule;
    }

    public void save(Status modelType, File file) {
        StatusType statusType = convertTo(modelType);
        JAXBElement<StatusType> jaxbElement = objectFactory.createStatus(statusType);
        jaxbTools.marshallXML(jaxbElement, file);
    }

    @Override
    public Status convertFrom(StatusType xmlType) {
        Status status = new Status();
        for (SessionType sessionType : xmlType.getSession()) {
            LocalDate date = DateTimeUtils.toLocalDate(sessionType.getWhen());
            LocalTime time = DateTimeUtils.toLocalTime(sessionType.getWhen());
            Showing showing = schedule.searchShowing(date, time, sessionType.getRoom());
            Session session = new Session(showing);
            for (ReservationType reservationType : sessionType.getReservation()) {
                Reservation reservation = new Reservation(
                        reservationType.getName(),
                        reservationType.getPhone(),
                        reservationType.getSeats()
                );
                session.addReservation(reservation);
            }
            boolean[][] occupation = session.getOccupation();
            for (TicketType ticketType : sessionType.getTicket()) {
                session.addTicket(ticketType.getRow(),ticketType.getSeat());
            }
            status.getSessions().add(session);
        }
        return status;
    }


    @Override
    public StatusType convertTo(Status modelType) {
        StatusType statusType = new StatusType();
        for (Session session : modelType.getSessions()) {
            SessionType sessionType = new SessionType();
            sessionType.setRoom(session.getShowing().getRoom().getName());
            LocalDateTime when = LocalDateTime.of(session.getShowing().getDate(), session.getShowing().getTime());
            sessionType.setWhen(DateTimeUtils.toXMLGregorianCalendar(when));
            List<ReservationType> sessionReservations = sessionType.getReservation();
            for (Reservation reservation : session.getReservations().values()) {
                ReservationType rt = new ReservationType();
                rt.setName(reservation.getName());
                rt.setPhone(reservation.getPhone());
                rt.setSeats(reservation.getSeats());
                sessionReservations.add(rt);
            }
            boolean[][] occupation = session.getOccupation();
            List<TicketType> sessionTickets = sessionType.getTicket();
            for (int i = 0; i < session.getRows(); i++) {
                for (int j = 0; j < session.getColumns(); j++) {
                    if (occupation[i][j]) {
                        TicketType ticketType = new TicketType();
                        ticketType.setRow(i);
                        ticketType.setSeat(j);
                        sessionTickets.add(ticketType);
                    }
                }
            }
            statusType.getSession().add(sessionType);
        }
        return statusType;
    }
}

package cinemafx.persistence.io;

import cinemafx.model.Movie;
import cinemafx.model.Room;
import cinemafx.model.Schedule;
import cinemafx.model.Showing;
import cinemafx.persistence.jaxb.schedule.MovieType;
import cinemafx.persistence.jaxb.schedule.RoomType;
import cinemafx.persistence.jaxb.schedule.ScheduleType;
import cinemafx.persistence.jaxb.schedule.ShowingType;
import cinemafx.util.DateTimeUtils;
import javafx.scene.image.Image;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 30/03/2017.
 */
public class ScheduleFileDriver extends AbstractFileDriver<ScheduleType, Schedule> {

    public ScheduleFileDriver() {
        super("cinemafx.persistence.jaxb.schedule", ScheduleType.class);
    }


    @Override
    public Schedule convertFrom(ScheduleType xmlType) {
        Schedule schedule = new Schedule();
        for (MovieType movieType : xmlType.getMovies().getMovie()) {
            Movie movie = new Movie();
            movie.setCast(movieType.getCast());
            movie.setCountry(movieType.getCountry());
            movie.setDirector(movieType.getDirector());
            movie.setDuration(movieType.getDuration());
            movie.setTitle(movieType.getTitle());
            movie.setYear(movieType.getYear());
            movie.setPoster(new Image(ClassLoader.getSystemResourceAsStream("images/" + movieType.getPoster())));
            schedule.getMovies().put(movie.getTitle(), movie);
        }
        for (RoomType roomType : xmlType.getRooms().getRoom()) {
            Room room = new Room(roomType.getName(), roomType.getRows(), roomType.getSeats());
            schedule.getRooms().put(room.getName(), room);
        }
        for (ShowingType showingType : xmlType.getShowings().getShowing()) {
            Showing showing = new Showing();
            showing.setDate(DateTimeUtils.toLocalDate(showingType.getTime()));
            showing.setTime(DateTimeUtils.toLocalTime(showingType.getTime()));
            showing.setRoom(schedule.getRooms().get(showingType.getRoom()));
            showing.setMovie(schedule.getMovies().get(showingType.getMovie()));
            schedule.addShowing(showing);
        }
        return schedule;
    }


    @Override
    protected ScheduleType convertTo(Schedule modelType) {
        return null;
    }
}

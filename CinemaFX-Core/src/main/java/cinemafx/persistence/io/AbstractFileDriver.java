package cinemafx.persistence.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.InputStream;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 03/04/2017.
 */
public abstract class AbstractFileDriver<X, T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleFileDriver.class);
    protected JAXBTools<X> jaxbTools;

//    public AbstractFileDriver(Class<X> xmlRootClass) {
//        try {
//            jaxbTools = new JAXBTools<>(xmlRootClass);
//        } catch (JAXBException e) {
//            LOGGER.error("Error creating JAXBTools with context {}", ScheduleType.class);
//        }
//    }

    public AbstractFileDriver(String contextPath, Class<X> rootXmlClass) {
        try {
            jaxbTools = new JAXBTools<>(contextPath, rootXmlClass);
        } catch (JAXBException e) {
            LOGGER.error("Error creating JAXBTools with context {}", contextPath);
        }
    }

    public T load(File file) {
        X jaxbType = jaxbTools.unmarshallFile(file);
        return (jaxbType != null ? convertFrom(jaxbType) : null);
    }

    public T load(InputStream is) {
        X jaxbType = jaxbTools.unmarshallStream(is);
        return (jaxbType != null ? convertFrom(jaxbType) : null);
    }

    public void save(T modelType, File file) {
        jaxbTools.marshallXML(convertTo(modelType), file);
    }

    protected abstract T convertFrom(X xmlType);

    protected abstract X convertTo(T modelType);

}

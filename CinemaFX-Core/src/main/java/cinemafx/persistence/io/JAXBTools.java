package cinemafx.persistence.io;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.*;
import javax.xml.transform.stream.StreamSource;
import java.io.*;


public class JAXBTools<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(JAXBTools.class);
    private final Marshaller marshaller;
    private final Unmarshaller unmarshaller;
    private final JAXBContext jaxbContext;
    private final Class<T> rootClass;

//    public JAXBTools(Class<T> contextClass) throws JAXBException {
//        JAXBContext jaxbContext = JAXBContext.newInstance(contextClass);
//        unmarshaller = jaxbContext.createUnmarshaller();
//        marshaller = jaxbContext.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        this.rootClass = contextClass;
//    }

    public JAXBTools(String contextPath, Class<T> rootClass) throws JAXBException {
        //JAXBContext jc = JAXBContext.newInstance("jaxb.package", jaxb.package.ObjectFactory.class.getClassLoader());
        jaxbContext = JAXBContext.newInstance(contextPath);
        unmarshaller = jaxbContext.createUnmarshaller();
        marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        this.rootClass = rootClass;
    }


    /**
     * Unmarshalls XML element from file into java object
     *
     * @param file the XML file to be unmarshalled
     * @return the object unmarshalled from the {@code file}
     */
    public T unmarshallFile(File file) {
        T object = null;
        try {
            object = unmarshallStream(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            LOGGER.error("File {} not found", file);
            LOGGER.error(e.getMessage());
            ;
        }
        return object;
    }

    /**
     * Unmarshalls XML element from InputStream into java object
     *
     * @param inputStream the input stream to be unmarshalled
     * @return the object unmarshalled from the {@code inputStream}
     */
    public T unmarshallStream(InputStream inputStream) {
        T object = null;
        try {
            JAXBElement<T> root = unmarshaller.unmarshal(new StreamSource(inputStream), rootClass);
            object = root.getValue();
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception unmarshalling from input stream");
            LOGGER.error(ex.getMessage());
        }
        return object;
    }

    /**
     * Marshalls Java object into XML file
     *
     * @param object object to be marshalled
     * @param file   file to save the marshalled object
     * @return the XML file
     */
    public File marshallXML(T object, File file) {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            marshaller.marshal(object, fos);
            return file;
        } catch (IOException ex) {
            LOGGER.error("IO exception marshalling to {}", file);
            LOGGER.error(ex.getMessage());
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception marshalling to {}", file);
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    /**
     * Marshalls Java object into XML file
     *
     * @param jaxbElement object to be marshalled
     * @param file   file to save the marshalled object
     * @return the XML file
     */
    public File marshallXML(JAXBElement<T> jaxbElement, File file) {
        try (FileOutputStream fos = new FileOutputStream(file)) {
            marshaller.marshal(jaxbElement, fos);
            return file;
        } catch (IOException ex) {
            LOGGER.error("IO exception marshalling to {}", file);
            LOGGER.error(ex.getMessage());
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception marshalling to {}", file);
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    public StringWriter marshallXML(T object) {
        try {
            try (StringWriter sw = new StringWriter()) {
                marshaller.marshal(object, sw);
                return sw;
            } catch (IOException ex) {
                LOGGER.error("IO exception marshalling to String Writer");
                LOGGER.error(ex.getMessage());
            }
        } catch (JAXBException ex) {
            LOGGER.error("JAXB exception marshalling to String Writer");
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

}

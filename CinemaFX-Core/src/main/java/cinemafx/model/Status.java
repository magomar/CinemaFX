package cinemafx.model;

import java.util.*;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 31/03/2017.
 */
public class Status {
//    Map<String, Set<Reservation>> reservations = new HashMap<>();
    List<Session> sessions = new ArrayList<>();

//    public Set<Reservation> getReservationsPerPerson(String person) {
//        if (!reservations.containsKey(person)) reservations.put(person, new HashSet<>());
//        return reservations.get(person);
//    }


    public List<Session> getSessions() {
        return sessions;
    }
}

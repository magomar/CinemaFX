package cinemafx.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 04/04/2017.
 */
public class Session {
    private final Showing showing;
    private final Map<String, Reservation> reservations = new HashMap<>();
    private final boolean[][] occupation;
    private final int rows;
    private final int columns;
    private int availability;

    public Session(Showing showing) {
        this.showing = showing;
        Room room = showing.getRoom();
        rows = room.getRows();
        columns = room.getSeats();
        availability = rows * columns;
        occupation = new boolean[rows][columns];
    }

    public Showing getShowing() {
        return showing;
    }

    public Map<String, Reservation> getReservations() {
        return reservations;
    }

    public boolean[][] getOccupation() {
        return occupation;
    }

    public void addReservation(Reservation reservation) {
        reservations.put(reservation.getName(), reservation);
        availability -= reservation.getSeats();
    }

    public int getAvailability() {
        return availability;
    }

    public boolean sell(Reservation reservation, Collection<Seat> seats) {
        if (seats.size() != reservation.getSeats()) return false;
        reservations.remove(reservation.getName());
        for (Seat seat : seats) {
            occupation[seat.getRow()][seat.getColumn()] = true;
        }
        return true;
    }

    public boolean updateSeat(int row, int column, boolean newValue) {
        boolean currentValue = occupation[row][column];
        if (currentValue != newValue) {
            occupation[row][column] = newValue;
            if (newValue == true) availability--;
            else availability++;
        }
        return true;
    }

    public void updateSeats(Seat[][] seats) {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                updateSeat(i,j, seats[i][j].isOccupied());
            }
        }
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }


    public LocalDate getDate() {
        return showing.getDate();
    }

    public LocalTime getTime() {
        return showing.getTime();
    }

    public Movie getMovie() {
        return showing.getMovie();
    }

    public Room getRoom() {
        return showing.getRoom();
    }

    public void addTicket(int row, int column) {
        occupation[row][column] = true;
        availability--;
    }
}

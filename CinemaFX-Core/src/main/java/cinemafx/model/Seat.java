package cinemafx.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 30/03/2017.
 */
public class Seat {
    private int row;
    private int column;
    private BooleanProperty occupied = new SimpleBooleanProperty(false);

    public Seat(int row, int column) {
        this(row,column,false);
    }

    public Seat(int row, int column, boolean occupied) {
        this.row = row;
        this.column = column;
        this.occupied = new SimpleBooleanProperty(occupied);
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean isOccupied() {
        return occupied.get();
    }

    public BooleanProperty occupiedProperty() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied.set(occupied);
    }
}

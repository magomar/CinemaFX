package cinemafx.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 20/03/2017.
 */
public class Schedule {
    private Map<String, Room> rooms = new HashMap<>();
    private Map<String, Movie> movies = new HashMap();
    private Map<LocalDate, List<Showing>> showingsPerDay = new HashMap<>();
    private Map<Movie, List<Showing>> showingsPerMovie = new HashMap<>();
    private List<Showing> showings = new ArrayList<>();

    public Map<String, Room> getRooms() {
        return rooms;
    }

    public Map<String, Movie> getMovies() {
        return movies;
    }

    public List<Showing> getShowingsPerDay(LocalDate date) {
        if (!showingsPerDay.containsKey(date)) showingsPerDay.put(date, new ArrayList<>());
        return showingsPerDay.get(date);
    }

    public List<Showing> getShowingsPerMovie(Movie movie) {
        if (!showingsPerMovie.containsKey(movie)) showingsPerMovie.put(movie, new ArrayList<>());
        return showingsPerMovie.get(movie);
    }

    public List<Showing> getShowings() {
        return showings;
    }

    public void addShowing(Showing s) {
        showings.add(s);
        getShowingsPerDay(s.getDate()).add(s);
        getShowingsPerMovie(s.getMovie()).add(s);
    }

    public Showing searchShowing(LocalDate localDate, LocalTime localTime, String roomName) {
        for (Showing showing : getShowingsPerDay(localDate)) {
            if (showing.getRoom().getName().equals(roomName)
                    && showing.getTime().equals(localTime)) return showing;
        }
        return null;
    }
}

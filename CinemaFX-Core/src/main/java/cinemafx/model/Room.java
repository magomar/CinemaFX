package cinemafx.model;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 20/03/2017.
 */
public class Room {
    private int rows;
    private int seats;
    private String name;

    public Room(String name, int rows, int seats) {
        this.rows = rows;
        this.seats = seats;
        this.name = name;
    }

    public int getRows() {
        return rows;
    }

    public int getSeats() {
        return seats;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}

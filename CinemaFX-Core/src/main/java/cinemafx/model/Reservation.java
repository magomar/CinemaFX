package cinemafx.model;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 30/03/2017.
 */
public class Reservation {
    private String name;
    private String phone;
    private int seats;

    public Reservation(String name, String phone, int seats) {
        this.name = name;
        this.phone = phone;
        this.seats = seats;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }
}

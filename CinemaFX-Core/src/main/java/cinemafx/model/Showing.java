package cinemafx.model;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 20/03/2017.
 */
public class Showing {
    private LocalDate date;
    private LocalTime time;
    private Movie movie;
    private Room room;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Showing showing = (Showing) o;

        if (!date.equals(showing.date)) return false;
        if (!time.equals(showing.time)) return false;
        return room.equals(showing.room);
    }

    @Override
    public int hashCode() {
        int result = date.hashCode();
        result = 31 * result + time.hashCode();
        result = 31 * result + room.hashCode();
        return result;
    }
}

package cinemafx.model;

import javafx.scene.image.Image;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 20/03/2017.
 */
public class Movie {
    private String director;
    private int duration;
    private Image poster;
    private String country;
    private int year;
    private String cast;
    private String title;

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Image getPoster() {
        return poster;
    }

    public void setPoster(Image poster) {
        this.poster = poster;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }

    public String toMultilineString() {
        return "Title: " + title + '\n' +
                "Director: " + director + '\n' +
                "Year: " + year + '\n' +
                "Country: " + country + '\n' +
                "Cast: " + cast + '\n' +
                "Duration: " + duration +  " minutes \n";
    }
}

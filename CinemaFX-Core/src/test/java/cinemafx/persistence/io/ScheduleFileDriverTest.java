package cinemafx.persistence.io;

import cinemafx.model.Schedule;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;

import static org.junit.Assert.assertEquals;

/**
 * @author <a href="mailto:magomar@gmail.com">Mario Gómez</a>
 *         Created on 03/04/2017.
 */
public class ScheduleFileDriverTest {
    private ScheduleFileDriver driver;
    private ClassLoader classLoader;

    @Before
    public void setUp() throws Exception {
        driver = new ScheduleFileDriver();
//        classLoader = getClass().getClassLoader();
        classLoader = ClassLoader.getSystemClassLoader();
    }

    @After
    public void tearDown() throws Exception {
        driver = null;
        classLoader = null;
    }

    @Test
    public void load() throws Exception {
//        InputStream inputStream = ClassLoader.getSystemClassLoader().getSystemResourceAsStream();
        InputStream is = classLoader.getResourceAsStream("data/schedule.xml");
        Schedule schedule = driver.load(is);
        assertEquals(schedule.getRooms().size(), 6);
        assertEquals(schedule.getMovies().size(), 6);
        assertEquals(schedule.getShowings().size(), 216);
    }

}